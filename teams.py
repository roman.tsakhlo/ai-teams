from logic import *

real = Symbol('Real') # Spanish
milan = Symbol('Milan') # Italian
metalist = Symbol('Metalist') # Ukrainian

antonio = Symbol('Antonio') # Italian
rodrigo = Symbol('Rodrigo') # Spanish
mykola = Symbol('Mykola') # Ukrainian

teams = [real, milan, metalist]
coaches = [antonio, rodrigo, mykola]

notSameNationalitySpanish = Not(And(rodrigo, real))
notSameNationalityItalian = Not(And(antonio, milan)) 
notSameNationalityUkrainian = Not(And(mykola, metalist))

NotSameNationality = Or(notSameNationalitySpanish, notSameNationalityItalian, notSameNationalityUkrainian) # Not same nationality

ImplicationAntonio = Implication(antonio, Not(metalist)) # Antonio is not a coach of Metalist 
ImplicationMykola = Implication(mykola, real) # Real promissed to not take Mykola
# RewriteOrFromImplicationAntonio = Or(Not(antonio), metalist)  
# RewriteOrFromImplicationMykola = Or(Not(mykola), real) 

knowlegde = And(
    NotSameNationality 
    # ImplicationAntonio, # or use RewriteOrFromImplicationAntonio
    # ImplicationMykola # or use RewriteOrFromImplicationMykola
) 

knowlegde.add(And(ImplicationAntonio, ImplicationMykola))
# knowlegde.add(And(RewriteOrFromImplicationAntonio, RewriteOrFromImplicationMykola))

print(knowlegde.formula())


# ------------ Implementation logic ---------------

def checkCoachOfTeam(teams, coaches, checkingTeam):
    coachOfTeam = None

    for coach in coaches:
        for team in teams:

            if coach == antonio and team == metalist:
                continue # Antonio is not a coach of Metalist 

            if coach == mykola and team == real: 
                continue # Real promissed to not take Mykola 

            if (coach == antonio and team == milan) or (coach == rodrigo and team == real) or (coach == mykola and team == metalist):
                continue # Not same nationality

            if team == checkingTeam:
                coachOfTeam = coach 

    if coachOfTeam: 
        print(f"Coach of the team {checkingTeam} is {coachOfTeam}!")
    else:
        print('There is no search result...')


checkCoachOfTeam(teams, coaches, metalist) # Check for "Metalist"
checkCoachOfTeam(teams, coaches, real) # Check for "Real"
checkCoachOfTeam(teams, coaches, milan) # Check for "Milan"

